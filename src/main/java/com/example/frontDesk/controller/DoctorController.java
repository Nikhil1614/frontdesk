package com.example.frontDesk.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.frontDesk.DTO.DoctorRequestDTO;
import com.example.frontDesk.DTO.DoctorResponse;
import com.example.frontDesk.domain.Doctor;
import com.example.frontDesk.service.DoctorService;

@RestController
@CrossOrigin
@RequestMapping("/doctor")
public class DoctorController {

	@Autowired
	DoctorService doctorService;
	
	@PostMapping("/save")
	public DoctorResponse saveDoctor(@RequestBody DoctorRequestDTO doc) {
		
		return doctorService.saveDoctor(doc);
	}
	@GetMapping("/all")
	public List<DoctorResponse> getAllDoctors(){
		
		return doctorService.getDoctorsList();
		
	}
	@PutMapping("/update/{id}")
	public DoctorResponse updateDoctor(@RequestBody DoctorRequestDTO doc,@PathVariable("id") Long doctorId) {
		
		return doctorService.updateDoctor(doc,doctorId);
	}
	
	@GetMapping("/{id}")
	public DoctorResponse getDoctorById(@PathVariable("id") Long doctorId){
		
		return doctorService.getDoctorById(doctorId);
		
	}
	@DeleteMapping("/{id}")
	public void deleteDoctorById(@PathVariable("id") Long doctorId){
		
		 doctorService.deleteById(doctorId);
		
	}
}
