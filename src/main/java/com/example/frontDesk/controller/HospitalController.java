package com.example.frontDesk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.frontDesk.DTO.HospitalDetailsDTO;
import com.example.frontDesk.messagebuilder.HospitalMessageBuilder;
import com.example.frontDesk.repository.HospitalRepository;



@RestController
@CrossOrigin
@RequestMapping("/hospital")
public class HospitalController {

	@Autowired
	HospitalRepository hospitalRepository;
	@Autowired
	HospitalMessageBuilder hospitalMessageBuilder;
	@GetMapping()
	public HospitalDetailsDTO getHospitalDetails() {
		return hospitalMessageBuilder.toDTO(hospitalRepository.findAll().get(0));
	}
}
