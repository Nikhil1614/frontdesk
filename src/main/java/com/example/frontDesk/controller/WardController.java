package com.example.frontDesk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.frontDesk.DTO.WardListDTO;
import com.example.frontDesk.service.WardService;

@RestController
@CrossOrigin
@RequestMapping("/ward")
public class WardController {

	@Autowired
	WardService wardService;
	
	@GetMapping("/all")
	public List<WardListDTO> getAllWardDetails() {
		return wardService.getAllWards();
	}
}
