package com.example.frontDesk.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.frontDesk.DTO.LoginResponse;
import com.example.frontDesk.domain.LoginRequest;
import com.example.frontDesk.service.LoginService;

@RestController
@CrossOrigin
@RequestMapping("/welcome")
public class WelcomeController {

	@Autowired
	LoginService loginService;
	@GetMapping()
	public String helloWorld() {
		return "Up/Running";
	}
	
	@PostMapping("/login")
	public LoginResponse getLoginDetails(@RequestBody LoginRequest request) {
		
		return loginService.getLoginDetails(request);
	}
	
}
