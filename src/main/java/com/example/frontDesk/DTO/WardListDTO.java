package com.example.frontDesk.DTO;

import java.io.Serializable;
import java.util.List;


public class WardListDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private Long wardId;
    
  
	private String wardType;
    
    
	private Long totalBeds;
    
    
	private Long availableBeds;
	
	private List<DoctorWardDetails> doctorsList;


	


	public WardListDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public WardListDTO(Long wardId, String wardType, Long totalBeds, Long availableBeds,
			List<DoctorWardDetails> doctorsList) {
		super();
		this.wardId = wardId;
		this.wardType = wardType;
		this.totalBeds = totalBeds;
		this.availableBeds = availableBeds;
		this.doctorsList = doctorsList;
	}


	public List<DoctorWardDetails> getDoctorsList() {
		return doctorsList;
	}


	public void setDoctorsList(List<DoctorWardDetails> doctorsList) {
		this.doctorsList = doctorsList;
	}


	public Long getWardId() {
		return wardId;
	}


	public void setWardId(Long wardId) {
		this.wardId = wardId;
	}


	public String getWardType() {
		return wardType;
	}


	public void setWardType(String wardType) {
		this.wardType = wardType;
	}


	public Long getTotalBeds() {
		return totalBeds;
	}


	public void setTotalBeds(Long totalBeds) {
		this.totalBeds = totalBeds;
	}


	public Long getAvailableBeds() {
		return availableBeds;
	}


	public void setAvailableBeds(Long availableBeds) {
		this.availableBeds = availableBeds;
	}
    
    
}
