package com.example.frontDesk.DTO;

import java.io.Serializable;

import javax.persistence.Column;

public class HospitalDetailsDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private Long hospitalId;
	
	
	private String hospName;
	
	
	private Long availableBeds;
	

	private Long totalBeds;


	public HospitalDetailsDTO(Long hospitalId, String hospName, Long availableBeds, Long totalBeds) {
		super();
		this.hospitalId = hospitalId;
		this.hospName = hospName;
		this.availableBeds = availableBeds;
		this.totalBeds = totalBeds;
	}


	public HospitalDetailsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getHospitalId() {
		return hospitalId;
	}


	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}


	public String getHospName() {
		return hospName;
	}


	public void setHospName(String hospName) {
		this.hospName = hospName;
	}


	public Long getAvailableBeds() {
		return availableBeds;
	}


	public void setAvailableBeds(Long availableBeds) {
		this.availableBeds = availableBeds;
	}


	public Long getTotalBeds() {
		return totalBeds;
	}


	public void setTotalBeds(Long totalBeds) {
		this.totalBeds = totalBeds;
	}
	
	

}
