package com.example.frontDesk.DTO;

import java.io.Serializable;

public class DoctorRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long doctorId;

	private String fName;

	private String lName;

	private String mobileNum;

	private String emailId;

	private String specialization;

	private Boolean isAvailable;

	private Long wardId;

	public DoctorRequestDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DoctorRequestDTO(Long doctorId, String fName, String lName, String mobileNum, String emailId,
			String specialization, Boolean isAvailable, Long wardId) {
		super();
		this.doctorId = doctorId;
		this.fName = fName;
		this.lName = lName;
		this.mobileNum = mobileNum;
		this.emailId = emailId;
		this.specialization = specialization;
		this.isAvailable = isAvailable;
		this.wardId = wardId;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public Boolean getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public Long getWardId() {
		return wardId;
	}

	public void setWardId(Long wardId) {
		this.wardId = wardId;
	}

}
