package com.example.frontDesk.DTO;

import java.io.Serializable;

public class ResponseStatus<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String statusCode;
	private String message;
	private T response;
	public ResponseStatus(String statusCode, String message, T response) {
		super();
		this.statusCode = statusCode;
		this.message = message;
		this.response = response;
	}
	public ResponseStatus() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getResponse() {
		return response;
	}
	public void setResponse(T response) {
		this.response = response;
	}
	
}
