package com.example.frontDesk.messagebuilder;

import org.springframework.stereotype.Component;

import com.example.frontDesk.DTO.HospitalDetailsDTO;
import com.example.frontDesk.domain.Hospital;

@Component
public class HospitalMessageBuilder {

	public HospitalDetailsDTO toDTO(Hospital hosp) {
		HospitalDetailsDTO dto=new HospitalDetailsDTO();
		dto.setHospName(hosp.getHospName());
		dto.setHospitalId(hosp.getHospitalId());
		dto.setAvailableBeds(hosp.getAvailableBeds());
		dto.setTotalBeds(hosp.getTotalBeds());
		return dto;
	}
}
