package com.example.frontDesk.messagebuilder;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.frontDesk.DTO.DoctorRequestDTO;
import com.example.frontDesk.DTO.DoctorResponse;
import com.example.frontDesk.DTO.DoctorWardDetails;
import com.example.frontDesk.domain.Appointment;
import com.example.frontDesk.domain.Doctor;
import com.example.frontDesk.domain.Ward;

@Component
public class DoctorMessageBuilder {

	public DoctorResponse buildDoctorResponse(Doctor d,Ward w,List<Appointment> a) {
		DoctorResponse docRes=new DoctorResponse();
		docRes.setDoctorId(d.getDoctorId());
		docRes.setEmailId(d.getEmailId());
		docRes.setfName(d.getfName());
		docRes.setlName(d.getlName());
		docRes.setIsAvailable(d.getIsAvailable());
		docRes.setMobileNum(d.getMobileNum());
		docRes.setSpecialization(d.getSpecialization());
		docRes.setWardId(w.getWardId());
		docRes.setWardType(w.getWardType());
		docRes.setAppointmentId(null);
		return docRes;
	}

	public Doctor buildDoctorSave(DoctorRequestDTO d, Ward wa) {
		wa.setWardId(d.getWardId());
		Doctor docRes=new Doctor();
		docRes.setDoctorId(d.getDoctorId());
		docRes.setEmailId(d.getEmailId());
		docRes.setfName(d.getfName());
		docRes.setlName(d.getlName());
		docRes.setIsAvailable(d.getIsAvailable());
		docRes.setMobileNum(d.getMobileNum());
		docRes.setSpecialization(d.getSpecialization());
		docRes.setWardAssigned(wa);
		return docRes;
	}
	
	public List<DoctorWardDetails> buildWardDoctors(List<Doctor> docList){
		List<DoctorWardDetails> res=new ArrayList<>();
		docList.stream().forEach(doc->{
			DoctorWardDetails docWard=new DoctorWardDetails();
			docWard.setDoctorId(doc.getDoctorId());
			docWard.setfName(doc.getfName());
			docWard.setlName(doc.getlName());
			docWard.setEmailId(doc.getEmailId());
			docWard.setMobileNum(doc.getMobileNum());
			docWard.setIsAvailable(doc.getIsAvailable());
			docWard.setSpecialization(doc.getSpecialization());
			res.add(docWard);
		});
		return res;
	}
}
