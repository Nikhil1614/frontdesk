package com.example.frontDesk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.frontDesk.domain.LoginRequest;

@Repository
public interface LoginRequestRepository extends JpaRepository<LoginRequest,Long>{

}
