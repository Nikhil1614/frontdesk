package com.example.frontDesk.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.frontDesk.domain.Ward;

public interface WardRepository extends JpaRepository<Ward,Long>{

}
