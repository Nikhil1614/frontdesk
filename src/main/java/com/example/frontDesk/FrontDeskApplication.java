package com.example.frontDesk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class FrontDeskApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrontDeskApplication.class, args);
	}

	
}
