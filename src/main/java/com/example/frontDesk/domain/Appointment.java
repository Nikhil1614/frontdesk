package com.example.frontDesk.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name="APPOINTMENT")
@JsonInclude(value=JsonInclude.Include.ALWAYS)
public class Appointment implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="APPO_ID")
	private Long appId;
	
	@Column(name="IS_VALID")
	private Boolean isValid;
	
	@Column(name="APPO_DATE")
	private String appointmentDate;
	
	@ManyToOne()
	@JoinColumn(name="DOC_ID")
	private Doctor doctorDetails;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PATIENT_ID")
	private Patients patientDetails;
	
	public Long getAppId() {
		return appId;
	}
	public void setAppId(Long appId) {
		this.appId = appId;
	}
	public Boolean getIsValid() {
		return isValid;
	}
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	public String getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	public Doctor getDoctorDetails() {
		return doctorDetails;
	}
	public void setDoctorDetails(Doctor doctorDetails) {
		this.doctorDetails = doctorDetails;
	}
	public Patients getPatientDetails() {
		return patientDetails;
	}
	public void setPatientDetails(Patients patientDetails) {
		this.patientDetails = patientDetails;
	}
	
	
}
