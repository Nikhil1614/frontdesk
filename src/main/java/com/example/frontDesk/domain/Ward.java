package com.example.frontDesk.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name="WARD")
@JsonInclude(value=JsonInclude.Include.ALWAYS)
public class Ward implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    @Column(name="WARD_ID")
	private Long wardId;
    
    @Column(name="WARD_TYPE")
	private String wardType;
    
    @Column(name="TOTAL_BEDS")
	private Long totalBeds;
    
    @Column(name="AVAIL_BEDS")
	private Long availableBeds;
    
    @OneToMany(mappedBy="wardAssigned")
	private List<Doctor> assignedDoctors;
    
    @OneToMany(mappedBy="patientsWard")
	private List<Patients> assignedPatients;

	public Long getWardId() {
		return wardId;
	}

	public void setWardId(Long wardId) {
		this.wardId = wardId;
	}

	public String getWardType() {
		return wardType;
	}

	public void setWardType(String wardType) {
		this.wardType = wardType;
	}

	public Long getTotalBeds() {
		return totalBeds;
	}

	public void setTotalBeds(Long totalBeds) {
		this.totalBeds = totalBeds;
	}

	public Long getAvailableBeds() {
		return availableBeds;
	}

	public void setAvailableBeds(Long availableBeds) {
		this.availableBeds = availableBeds;
	}

	public List<Doctor> getAssignedDoctors() {
		return assignedDoctors;
	}

	public void setAssignedDoctors(List<Doctor> assignedDoctors) {
		this.assignedDoctors = assignedDoctors;
	}

	public List<Patients> getAssignedPatients() {
		return assignedPatients;
	}

	public void setAssignedPatients(List<Patients> assignedPatients) {
		this.assignedPatients = assignedPatients;
	}
    
    
}
