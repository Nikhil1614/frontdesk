package com.example.frontDesk.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="PATIENTS")
public class Patients {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="PAT_ID")
	private Long patientId;
	
	@Column(name="FIRST_NAME")
	private String fName;
	
	@Column(name="LAST_NAME")
	private String lName;
	
	@Column(name="MOB_NUM")
	private String mobileNum;
	
	@Column(name="EMAIL_ID")
	private String emailId;
	
	@Column(name="THERAPY")
	private String therapy;
	
	@Column(name="COMMENT")
	private String comments;
	
	@Column(name="PAT_TYPE")
	private String patientType;
	
	@Column(name="BED_NEED")
	private boolean isBedNeed;
	
	@OneToOne(mappedBy="patientDetails")
	private Appointment fixedAppointment;
	
	@ManyToOne()
	@JoinColumn(name="WARD_ID")
	private Ward patientsWard;

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getTherapy() {
		return therapy;
	}

	public void setTherapy(String therapy) {
		this.therapy = therapy;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPatientType() {
		return patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	public boolean isBedNeed() {
		return isBedNeed;
	}

	public void setBedNeed(boolean isBedNeed) {
		this.isBedNeed = isBedNeed;
	}

	public Appointment getFixedAppointment() {
		return fixedAppointment;
	}

	public void setFixedAppointment(Appointment fixedAppointment) {
		this.fixedAppointment = fixedAppointment;
	}

	public Ward getPatientsWard() {
		return patientsWard;
	}

	public void setPatientsWard(Ward patientsWard) {
		this.patientsWard = patientsWard;
	}

	
	
	
	
}
