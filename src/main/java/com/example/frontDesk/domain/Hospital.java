package com.example.frontDesk.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="HOSPITAL")
public class Hospital {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="HOSP_ID")
	private Long hospitalId;
	
	@Column(name="HOSP_NAME")
	private String hospName;
	
	@Column(name="AVAIL_BEDS")
	private Long availableBeds;
	
	@Column(name="TOTAL_BEDS")
	private Long totalBeds;
	
	
	public Long getTotalBeds() {
		return totalBeds;
	}
	public void setTotalBeds(Long totalBeds) {
		this.totalBeds = totalBeds;
	}
	public Long getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospName() {
		return hospName;
	}
	public void setHospName(String hospName) {
		this.hospName = hospName;
	}
	public Long getAvailableBeds() {
		return availableBeds;
	}
	public void setAvailableBeds(Long availableBeds) {
		this.availableBeds = availableBeds;
	}

	
	
	

}
