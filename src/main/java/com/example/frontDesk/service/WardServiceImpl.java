package com.example.frontDesk.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.frontDesk.DTO.WardListDTO;
import com.example.frontDesk.domain.Doctor;
import com.example.frontDesk.domain.Ward;
import com.example.frontDesk.messagebuilder.DoctorMessageBuilder;
import com.example.frontDesk.repository.WardRepository;

@Service
@Transactional
public class WardServiceImpl implements WardService {

	@Autowired
	WardRepository wardRepository;
	
	@Autowired
	DoctorMessageBuilder msgBuilder;
	
	@Override
	public List<WardListDTO> getAllWards() {
		List<WardListDTO> resultList=new ArrayList<>();
		List<Ward> wardList=wardRepository.findAll();
		
		if(!wardList.isEmpty()) {
			resultList=wardList.stream().map(ward ->{
				List<Doctor> doctorsInWard=new ArrayList<>();
				WardListDTO res=new WardListDTO();
				res.setWardType(ward.getWardType());
				res.setAvailableBeds(ward.getAvailableBeds());
				res.setTotalBeds(ward.getTotalBeds());
				res.setWardId(ward.getWardId());
				doctorsInWard=ward.getAssignedDoctors();
				res.setDoctorsList(msgBuilder.buildWardDoctors(doctorsInWard));
				return res;
			}).collect(Collectors.toList());
		}
		return resultList;
	}

}
