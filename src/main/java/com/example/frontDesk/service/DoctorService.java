package com.example.frontDesk.service;

import java.util.List;

import com.example.frontDesk.DTO.DoctorRequestDTO;
import com.example.frontDesk.DTO.DoctorResponse;


public interface DoctorService {

	List<DoctorResponse> getDoctorsList();

	DoctorResponse saveDoctor(DoctorRequestDTO doc);

	DoctorResponse updateDoctor(DoctorRequestDTO doc, Long doctorId);

	DoctorResponse getDoctorById(Long doctorId);

	void deleteById(Long doctorId);

}
