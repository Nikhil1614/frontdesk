package com.example.frontDesk.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.frontDesk.DTO.LoginResponse;
import com.example.frontDesk.domain.LoginRequest;
import com.example.frontDesk.repository.LoginRequestRepository;

@Service
public class LoginService {

	@Autowired
	LoginRequestRepository userRepository;
	public LoginResponse getLoginDetails(LoginRequest request) {
		LoginResponse response=new LoginResponse();
		LoginRequest req= new LoginRequest();
		List<LoginRequest> users=userRepository.findAll();
		users=users.stream().filter(user-> ((user.getUserName().equalsIgnoreCase(request.getUserName()))&&(user.getPassword().equals(request.getPassword()))))
		.collect(Collectors.toList());
		if(users.isEmpty()) {
			response.setStatus("500");
			response.setMessage("FAIL");
		}
		else {
			req=users.get(0);
			response.setStatus("200");
			response.setMessage("SUCCESS");
			response.setUserId(req.getUserId());
			response.setUserName(req.getUserName());
		}
		return response;
	}

}
