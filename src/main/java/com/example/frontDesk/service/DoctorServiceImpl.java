package com.example.frontDesk.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.frontDesk.DTO.DoctorRequestDTO;
import com.example.frontDesk.DTO.DoctorResponse;
import com.example.frontDesk.domain.Doctor;
import com.example.frontDesk.domain.Ward;
import com.example.frontDesk.messagebuilder.DoctorMessageBuilder;
import com.example.frontDesk.repository.DoctorRepository;
import com.example.frontDesk.repository.WardRepository;

@Service
@Transactional
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	DoctorRepository doctorRepository;
	@Autowired
	WardRepository wardRepository;
	@Autowired
	DoctorMessageBuilder doctorMessageBuilder;

	@Override
	public List<DoctorResponse> getDoctorsList() {
		List<Doctor> totalDoctorList = new ArrayList<>();
		List<DoctorResponse> response = new ArrayList<>();
		totalDoctorList = doctorRepository.findAll();
		if (!totalDoctorList.isEmpty() || totalDoctorList != null) {
			totalDoctorList.stream().forEach(each -> {
				DoctorResponse res = new DoctorResponse();
				Ward wa = null;
				wa = each.getWardAssigned();
				res = doctorMessageBuilder.buildDoctorResponse(each, wa, null);
				response.add(res);
			});
		}

		return response;
	}

	@Override
	public DoctorResponse saveDoctor(DoctorRequestDTO doc) {
		Ward wa = new Ward();
		Doctor doctor = doctorMessageBuilder.buildDoctorSave(doc, wa);
		DoctorResponse res = new DoctorResponse();
		doctorRepository.save(doctor);
		wa = doctor.getWardAssigned();
		res = doctorMessageBuilder.buildDoctorResponse(doctor, wa, null);
		return res;
	}

	@Override
	public DoctorResponse updateDoctor(DoctorRequestDTO doc,Long doctorId) {

		Optional<Doctor> doctor = doctorRepository.findById(doctorId);
		Ward wa = new Ward();
		Doctor d = null;
		wa.setWardId(doc.getWardId());
		if (doctor.isPresent()) {

			d = doctorRepository.save(doctorMessageBuilder.buildDoctorSave(doc, wa));
			wa = d.getWardAssigned();

			return doctorMessageBuilder.buildDoctorResponse(d, wa, null);
		} else
			return null;
	}

	@Override
	public DoctorResponse getDoctorById(Long doctorId) {
		Optional<Doctor> doctor = doctorRepository.findById(doctorId);
		Ward wa = new Ward();
		if (doctor.isPresent()) {
			Doctor d = doctor.get();
			wa=d.getWardAssigned();
			return doctorMessageBuilder.buildDoctorResponse(d, wa, null);
		}
		else
		return null;
	}

	@Override
	public void deleteById(Long doctorId) {
		Optional<Doctor> doctor = doctorRepository.findById(doctorId);
		if (doctor.isPresent()) {
			doctorRepository.deleteById(doctorId);
		}
		
	}

}
