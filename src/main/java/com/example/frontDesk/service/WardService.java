package com.example.frontDesk.service;

import java.util.List;

import com.example.frontDesk.DTO.WardListDTO;

public interface WardService {

	List<WardListDTO> getAllWards();

}
